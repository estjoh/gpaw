.. _basics:

=================================
Basics
=================================

.. toctree::
   :maxdepth: 1

   gettingstarted/gettingstarted
   water/water
   aluminium/aluminium
   surface/surface
   ../H2/atomization
